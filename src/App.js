import React, { useContext } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import { Context } from './components/context';
import SearchInput from './components/search-bar';
import ResultsView from './components/results-list';
import Modal from './components/modal';

import './App.scss';

const App = () => {
  const { characters, modalOpen } = useContext(Context);
  return <Router>
    <div className="container">
      <SearchInput />
      <div id="modal-container">
        <ResultsView items={characters} />
        { modalOpen && <Modal /> }
      </div>
    </div>
  </Router>;
};

export default App;