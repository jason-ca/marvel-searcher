const baseUrl = 'https://gateway.marvel.com:443/v1';
const apiKey = '21670da8b48abb870bd9ab87107e32a8';

const characters = '/public/characters';
const byCharacterName = '?nameStartsWith=';

export const allCharactersUrl = `${baseUrl}${characters}?apikey=${apiKey}`;

const IMAGE_SIZES = {
  characterComic: 'portrait_small',
  character: 'portrait_incredible'
};

export const getCharacterComicsUrl = characterId =>
  `${baseUrl}${characters}/${characterId}/comics?orderBy=focDate&apikey=${apiKey}`;

export const getCharactersByNameUrl = query =>
  `${baseUrl}${characters}${byCharacterName}${encodeURIComponent(query)}&apikey=${apiKey}`;

export const getImageUrl = (url, ext, mode) => `${url}/${IMAGE_SIZES[mode]}.${ext}`;