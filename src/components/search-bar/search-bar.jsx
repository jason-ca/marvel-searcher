import React, { useContext } from 'react';

import Bookmarks from '../bookmarks';
import { Context } from '../context';

import logo from './marvel.svg';
import './styles.scss';

export default function SearchBar() {
  const searchBarContext = useContext(Context);
  const {
    handleChangeQuery,
    handleSumbitSearch,
    searchQuery
  } = searchBarContext;

  return (
    <div className="searchbar-container">
      <div className="logo-container">
        <img src={logo} className="logo" alt="Marvel logo" />
      </div>

      <div className="separator" />

      <input
        type="text"
        value={searchQuery}
        onChange={e => handleChangeQuery(e)}
        onKeyPress={e => handleSumbitSearch(e)}
        placeholder="Buscar, Enter to go"
      />

      <div className="bookmark-container">
        <Bookmarks entity={{ type: 'search', value: searchQuery }} />
      </div>

      <div className="separator" />
    </div>
  );
}
