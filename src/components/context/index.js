import React, { useEffect, useState } from 'react';

import {
  allCharactersUrl,
  getCharactersByNameUrl,
  getCharacterComicsUrl
} from '../../api';

export const Context = React.createContext(null);

export const Provider = props => {
  const [characterComics, setComics] = useState([]);
  const [activeCharacter, setCharacter] = useState('');
  const [characters, setCharacters] = useState([]);
  const [modalOpen, toggleModal] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');
  const [bookmarks, setBookmarks] = useState(
    JSON.parse(localStorage.getItem('userBookmarks')) || []
  );

  const fetchItems = async(url = allCharactersUrl, isComics = false) => {
    fetch(url)
      .then(response => {
        if(response.ok) return response.json();
        throw new Error('API error occured');
      })
      .then(response => {
        const { data: { results } } = response;

        if (url === allCharactersUrl && results.length > 0) {
          const randomCharacter = results[Math.floor(Math.random() * results.length)];
          return setCharacters([randomCharacter]);
        } else if (isComics) {
          setComics(results);
        } else {
          setCharacters(results);
        };
      })
      .catch(err => console.error);
  };

  const handleAddBookmark = entity => {
    setBookmarks(bookmarks => [...bookmarks, entity]);
  };

  const handleRemoveBookmark = entity => {
    setBookmarks(bookmarks => bookmarks.filter(item =>
      item.type !== entity.type && item.value !== entity.value
    ));
  };

  const handleChangeQuery = e => {
    setSearchQuery(e.target.value);
  };

  const handleSumbitSearch = e => {
    if (e.key === 'Enter') fetchItems(getCharactersByNameUrl(searchQuery));
  };

  const handleToggleModal = (characterId = null, name) => {
    if (characterId === null ) {
      setComics([]);
    } else {
      setCharacter(name);
      fetchItems(getCharacterComicsUrl(characterId), true);
    };
    toggleModal(!modalOpen);
  };

  useEffect(() => {
    fetchItems();
  }, []);

  useEffect(() => {
    localStorage.setItem('userBookmarks', JSON.stringify(bookmarks));
  }, [bookmarks]);

  return (
    <Context.Provider
      value={{
        characters,
        bookmarks,
        searchQuery,
        modalOpen,
        characterComics,
        activeCharacter,
        handleAddBookmark,
        handleRemoveBookmark,
        handleChangeQuery,
        handleSumbitSearch,
        handleToggleModal,
      }}
    >
      {props.children}
    </Context.Provider>
  );
};
