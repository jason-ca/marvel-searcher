import React, { useContext } from 'react';
import { createPortal } from 'react-dom';

import { Context } from '../context';
import ResultsList from '../results-list';

import './styles.scss';

export default function Modal() {
  const { handleToggleModal, characterComics, activeCharacter } = useContext(Context);

  const content = <div className="modal-overlay">
    <div className="modal">
      <div className="modal-header">
        <div className="modal-title">{activeCharacter}</div>
        <div className="close-control" onClick={() => handleToggleModal()}>X</div>
      </div>
      <div className="main-content">
        <ResultsList items={characterComics} comics />
      </div>
    </div>
  </div>;

  return createPortal(content, document.getElementById('modal-container'));
};