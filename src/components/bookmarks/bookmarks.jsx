import React, { useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar as solidStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as star } from '@fortawesome/free-regular-svg-icons';

import { Context } from '../context';

export default function Bookmarks({ entity }) {
  const {
    bookmarks,
    handleAddBookmark,
    handleRemoveBookmark,
  } = useContext(Context);
  
  const isBookmarked = bookmarks.find(item =>
    item.type === entity.type && item.value === entity.value
  );
    
  const bookmarkIcon = isBookmarked ? solidStar : star;
  const bookmarkIconStyle = isBookmarked ? { color: 'yellow ', cursor: 'pointer' } : { cursor: 'pointer'};
  const bookmarkAction = isBookmarked
    ? () => handleRemoveBookmark(entity)
    : () => handleAddBookmark(entity);

  return <FontAwesomeIcon
    icon={bookmarkIcon}
    onClick={bookmarkAction}
    style={bookmarkIconStyle}
  />;
};
