import React from 'react';

import Card from '../card';
import Bookmarks from '../bookmarks';
import { getImageUrl } from '../../api';

import './styles.scss';

const ResultsList = ({ comics = false, items }) => {
  const listClassName = comics ? 'comic-list' : 'card-list';
  
  return <div className={listClassName}>
    {items.map(result => {
      const {
        id,
        description,
        thumbnail: { path, extension },
        name,
        title,
      } = result;

      if (comics) {
        return <div className="comic-entry" key={id}>
          <div className="image-container">
            <img src={getImageUrl(path, extension, 'characterComic')} alt={title}/>
          </div>
          <div className="comic-text">
            <div className="comic-title">
              {title}
              <Bookmarks entity={{ type: 'comic', value: title }}/>
            </div>
            <div>
              {description}
            </div>
          </div>
        </div>
      };

      return <Card key={id} id={id} name={name} ext={extension} url={path} />;
  })}
  </div>
};

export default ResultsList;