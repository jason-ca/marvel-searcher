import React, { useContext } from 'react';

import { Context } from '../context';
import Bookmarks from '../bookmarks';
import { getImageUrl } from '../../api';

import './styles.scss';

export default function Card({ id, name, ext, url }) {
  const { handleToggleModal } = useContext(Context);

  return (
    <div className="card">
      <div className="image-overlay">
        <img
          onClick={() => handleToggleModal(id, name)}
          className="card-image"
          src={getImageUrl(url, ext, 'character')}
          alt={`${name}`}
        />
        <div className="overlay-top">
        </div>
        <div className="overlay-bottom">
          <div className="name">{name}</div>
          <Bookmarks entity={{type: 'name', value: name }} />
        </div>
      </div>
    </div>
  );
};